<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <form method="POST" action="">
        <figure id="team">
            <?php foreach ($afdelingen as $afdeling): ?>
                <p>afdeling <em><?= $afdeling->getNaam(); ?></em></p>
                <textarea rows="10" cols="100" name="bericht" name="omschrijving" required="bericht"><?= $afdeling->getOmschrijving(); ?></textarea><br>
                <input type="submit" value="Opslaan" name="verzenden"/>
            <?php endforeach; ?>
        </figure>
    </form>
</section>
<?php
include 'includes/footer.php';
